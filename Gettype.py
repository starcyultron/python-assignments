def getType(a,b,c,d,e):
	
	list1=[
	{ 'element': str(a), 'data-type ': str(type(a)), 'type' : 'numeric', 'is_mutable' : False},
	{ 'element': str(b), 'data-type ': str(type(b)), 'type' : 'sequence', 'is_mutable' : False,},
	{ 'element': str(c), 'data-type ': str(type(c)), 'type' : 'sequence', 'is_mutable' : True,},
	{ 'element': str(d), 'data-type ': str(type(d)), 'type' : 'sequence', 'is_mutable' : False,},
	{ 'element': str(e), 'data-type ': str(type(e)), 'type' : 'sequence', 'is_mutable' :True,}
	]

	return(list1)

print(getType(12,'two',['three'],('four',),{'five':''}))

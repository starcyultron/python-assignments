from copy import copy

def deepCopy(y):	
	y1=[]
	for i in y:
		if type(i) != int and type(i) != float and type(i) != str:
			y1.append(deepCopy(i))
		else:
			y1.append(i)
	return y1
y = [[1,2],'3',4]
res = deepCopy(y)
res[0][0]=2

print('Value of deepcopy after changes: ')
print(res)
print('Value of original list after changes in copied list: ')
print(y)


